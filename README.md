# DataVisualize-UsingReact-ChartJS

## Name

Data Visualization Project Using React JS , Node JS , Chart JS

## Instruction For Backend Folder

1. npm i
2. node index.js

## Instructions to Use

1.Select Tag Name
2.Select Starting Date&Time
3.Select Ending Date&Time
4.Select TimeZone
5.Click Submit Button

## All Listed Time Zones :

a. Pacific Zone ---> America/Los_Angeles,
b. Mountain Zone(Denver) ---> America/Denver,
c. Mountain Zone(Phoenix) --->America/Phoenix,
d. Central Zone ---> America/Chicago,
e. Eastern Zone --->America/New_York,
f. Local Zone ---> System Local Time Zone,

## Instructions to Use Chart

1.Reset Zoom --> Will reset the chart zoom.
2.Toggle Switch --> Can toggle the plot between absolute & normalised value. 3. Pan --> Shift+leftClick+Drag ---> pan in x axis.\ 4. xAxes --> Date&Time(timestamp) 5. yAxes --> Value 6. Zoom Controls : leftClick+Drag --> zoom x axes, 7. Hide Tag Plots : Click the tag name which is shown on the chart 8. Offset --> Increase / Decrease Time In minutes inside Details Options.\
