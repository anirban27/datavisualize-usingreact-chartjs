const express = require("express");
const axios = require("axios");
const app = express();
const moment = require("moment-timezone");
app.use(express.json());

const { readFileSync } = require("fs");
let timezoneList = () => JSON.parse(readFileSync("timeZone.json"));
let tagList = () => JSON.parse(readFileSync("tags.json"));

const API_ENDPOINT = "https://ngcfactoryeyeapi.azurewebsites.net/v1";

app.use("/taggraph", express.static("build"));

// Auth Settings
async function setAuth() {
	headers = {
		"X-Api-Key": "e7c713dc-8228-4c72-a71b-c36c4dbf6f8e",
	};
	return headers;
}
// URL Generator
function generateUrl(url, params) {
	url = API_ENDPOINT + url;
	// params.query.starttime = moment(
	// 	new Date(params.query.starttime).toISOString()
	// )
	// 	.tz(params.query.timezone)
	// 	.format(params.query.format);
	// params.query.endtime = moment(new Date(params.query.endtime).toISOString())
	// 	.tz(params.query.timezone)
	// 	.format(params.query.format);
	if (params && params.query) {
		const keys = Object.keys(params.query);
		for (let i = 0; i < keys.length; i++) {
			if (i === 0) {
				url += "?";
			}
			url += `${keys[i]}=${params.query[keys[i]]}`;
			if (i < keys.length - 1) {
				url += "&";
			}
		}
	}
	return url;
}
// Api Routes
app.get("/tagdata", async (req, res) => {
	var Result = {};
	var key = "Items";
	Result[key] = [];
	try {
		const headers = await setAuth();
		var url = generateUrl("/tagdata", req);
		const response = await axios.get(url, { headers: { ...headers } });
		response.data.result.sort(function (a, b) {
			return new Date(a.timestamp) - new Date(b.timestamp);
		});
		var min = Math.min.apply(
			null,
			response.data.result.map(item => item.numvalue)
		);
		var max = Math.max.apply(
			null,
			response.data.result.map(item => item.numvalue)
		);
		var diff = max - min;
		for (var i = 0; i < response.data.count; i++) {
			Result[key].push({
				// timestamp: response.data.result[i]["timestamp"],
				// zonetimestamp: moment(response.data.result[i]["timestamp"])
				// 	.tz(req.query.timezone)
				// 	.format("MM-DD-YYYY, HH:mm"),
				timestamp: moment(response.data.result[i]["timestamp"])
					.tz(req.query.timezone)
					.format("MM-DD-YYYY, HH:mm:ss"),
				tag: response.data.result[i]["tag"],
				rawvalue: response.data.result[i]["rawvalue"],
				numvalue: response.data.result[i]["numvalue"],
				numvaluenorm: (response.data.result[i]["numvalue"] - min) / diff,
			});
		}
	} catch (err) {
		Result[key] = [
			{
				massage: "Something went wrong. Check again...",
			},
		];
	}
	res.send(Result.Items);
});
app.get("/latesttagdata", async (req, res) => {
	const headers = await setAuth();
	var url = generateUrl("/latesttagdata", req);
	axios.get(url, { headers: { ...headers } }).then(response => {
		res.send(response.data);
	});
});

app.post("/latesttagdata", async (req, res) => {
	const headers = await setAuth();
	var url = generateUrl("/latesttagdata", req);
	axios
		.request({
			url: url,
			method: "post",
			headers: { ...headers },
			data: req.body,
		})
		.then(re => {
			res.send(re.data);
		});
});

app.get("/taglist", async (req, res) => {
	res.send(tagList());
});

app.get("/timezone", async (req, res) => {
	let timezoneLists = timezoneList();
	timezoneLists.push({
		id: timezoneLists.length + 1,
		zone: `Local TimeZone: ${moment.tz.guess()}`,
		timeZone: `${moment.tz.guess()}`,
	});
	res.send(timezoneLists);
});
//Server Listner
app.listen(3000, () => {
	console.log("Listening on http://localhost:3000/...");
});
