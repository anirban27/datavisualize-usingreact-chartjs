const fs = require("fs");
const path = require("path");
const fscpy = require("fs-extra");
//var html = fs.readFileSync('dist/boardprofile/index.html');
const backendBuildPath = path.join(__dirname, "./build");
const frontendBuildPath = path.join(__dirname, "../front-end/build");
// console.log(backendBuildPath);

fs.access(backendBuildPath, error => {
	if (error) {
		fs.mkdir(backendBuildPath, error => {
			if (error) {
				console.log("there is some error", err);
			} else {
				console.log("New Build dir Created!...");
				fileCpy();
			}
		});
	} else {
		console.log("Build Dir already exists !! Going next..");
		fileCpy();
	}
});

const fileCpy = () => {
	fscpy.copy(frontendBuildPath, backendBuildPath, err => {
		if (err) {
			console.log("An error occured while copying the folder.");
			return console.error(err);
		} else {
			console.log("Build copy completed!");
			readFile();
		}
	});
};

const readFile = () => {
	fs.readFile("build/index.html", "utf8", function (err, data) {
		if (err) {
			console.log("there is an Error");
			return;
		}
		var html = data;
		const currhtml = html.replace(/href="|src="/gi, str =>
			str.concat("/taggraph")
		);
		fs.writeFile(path.join("./build/index.html"), currhtml, err => {
			if (err) console.log(err);
			else {
				console.log("Build Successful!!");
			}
		});
	});
};

//html = html.split('<link rel="stylesheet" href="').join('<link rel="stylesheet" href="boardprofile/')
// htm = html
// 	.split('<link rel="stylesheet" href="')
// 	.join('<link rel="stylesheet" href="taggraph/');
