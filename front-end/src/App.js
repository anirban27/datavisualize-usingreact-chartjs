import "./App.css";
import DateTimePick from "./components/DateTimePick";

function App() {
	return (
		<div className="App">
			<DateTimePick />
		</div>
	);
}

export default App;
