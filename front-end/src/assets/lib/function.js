export function Median(data) {
	return Quartile_50(data);
}

export function Quartile_25(data) {
	return Quartile(data, 0.25);
}

export function Quartile_50(data) {
	return Quartile(data, 0.5);
}

export function Quartile_75(data) {
	return Quartile(data, 0.75);
}

export function Quartile(data, q) {
	data = Array_Sort_Numbers(data);
	var pos = (data.length - 1) * q;
	var base = Math.floor(pos);
	var rest = pos - base;
	if (data[base + 1] !== undefined) {
		return data[base] + rest * (data[base + 1] - data[base]);
	} else {
		return data[base];
	}
}

export function Array_Sort_Numbers(inputarray) {
	return inputarray.sort(function (a, b) {
		return a - b;
	});
}

export function Array_Sum(t) {
	return t.reduce(function (a, b) {
		return a + b;
	}, 0);
}

export function Array_Average(data) {
	return Array_Sum(data) / data.length;
}

export function Array_Stdev(tab) {
	var i,
		j,
		total = 0,
		mean = 0,
		diffSqredArr = [];
	for (i = 0; i < tab.length; i += 1) {
		total += tab[i];
	}
	mean = total / tab.length;
	for (j = 0; j < tab.length; j += 1) {
		diffSqredArr.push(Math.pow(tab[j] - mean, 2));
	}
	return Math.sqrt(
		diffSqredArr.reduce(function (firstEl, nextEl) {
			return firstEl + nextEl;
		}) / tab.length
	);
}

export const bgPlugin = {
	id: "background",
	beforeDraw: (chart, args, opts) => {
		if (!opts.color) {
			return;
		}

		const { ctx, chartArea } = chart;

		ctx.fillStyle = opts.color;
		ctx.fillRect(
			chartArea.left,
			chartArea.top,
			chartArea.width,
			chartArea.height
		);
	},
};
