import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Grid from "@mui/material/Grid";
import Alert from "@mui/material/Alert";
import CloseOutlinedIcon from "@mui/icons-material/CloseOutlined";
import RemoveCircleIcon from "@mui/icons-material/RemoveCircle";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import TuneIcon from "@mui/icons-material/Tune";
import moment from "moment";

const OffsetForm = props => {
	const [value, setValue] = useState(0);
	const { tagNameList, selectedTagName, submitHandler, startDate } = props;
	const [open, setOpen] = React.useState(false);
	const [reset, setreset] = useState(false);
	const [warningMsg, setwarningMsg] = useState({
		diff: 0,
		warning: false,
	});

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleSubmit = () => {
		submitHandler();
		setOpen(false);
	};
	const handleReset = () => {
		tagNameList.forEach(e => {
			e.offset = 0;
		});
		setreset(false);
	};

	const handleClose = (event, reason) => {
		if (reason && reason == "backdropClick") return;
		setOpen(false);
	};

	const handleSliderValue = (id, value) => {
		setreset(true);
		let val = "";
		if (value === "0-") {
			val = "-";
		} else {
			val = isNaN(parseInt(value)) ? 0 : parseInt(value);
		}

		tagNameList.forEach(e => {
			if (e.id === id) {
				e.offset = val;
				setValue(val);
			}
		});
	};

	const minusHandler = id => {
		setreset(true);
		tagNameList.forEach(e => {
			if (e.id === id) {
				e.offset = e.offset === undefined ? -1 : e.offset - 1;
				setValue(e.offset);
			}
		});
	};
	const plusHandler = id => {
		setreset(true);
		tagNameList.forEach(e => {
			if (e.id === id) {
				e.offset = e.offset === undefined ? 1 : e.offset + 1;
				setValue(e.offset);
			}
		});
	};

	const offSetDiffCalculate = () => {
		const currDate = moment().subtract(10, "minutes");
		const stDate = moment(startDate);
		const diff = currDate.diff(stDate, "minutes");
		diff < value
			? setwarningMsg({ diff, warning: true })
			: setwarningMsg({ diff, warning: false });
	};
	const checkOffsets = tagLists => {
		tagLists.forEach(ele => {
			if (ele.offset > 0) {
				setreset(true);
				return;
			}
		});
	};

	useEffect(() => {
		offSetDiffCalculate();
		checkOffsets(selectedTagName);
	}, [value, reset]);

	return (
		<div>
			<Button
				variant="outlined"
				size="small"
				onClick={handleClickOpen}
				disabled={selectedTagName.length > 1 ? false : true}
				sx={{
					height: "2rem",
					borderRadius: "1rem",
					fontWeight: 600,
					letterSpacing: "0.09em",
					padding: "0 1rem",
					fontSize: "0.87rem",
					border: "2px solid #1976d2",
				}}
			>
				Adjust Time Offset
				<TuneIcon fontSize="small" sx={{ ml: 1 }} />
			</Button>
			<Dialog open={open} onClose={handleClose}>
				<DialogTitle
					sx={{
						borderBottom: "2px solid #eee",
						position: "relative",
						padding: "0.5rem 2rem",
						mb: 1,
					}}
				>
					<div>Set Offset</div>
					<CloseOutlinedIcon
						onClick={handleClose}
						sx={{
							position: "absolute",
							right: "2%",
							top: "25%",
							cursor: "pointer",
						}}
					/>
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={2}>
						<Grid item xs={8}>
							<DialogContentText
								sx={{ fontWeight: 600, textAlign: "left", pl: 1 }}
							>
								Tag Names
							</DialogContentText>
						</Grid>
						<Grid item xs={4}>
							<DialogContentText
								sx={{ fontWeight: 600, textAlign: "left", pl: 0.2 }}
							>
								Offset in Minutes
							</DialogContentText>
						</Grid>
					</Grid>
				</DialogContent>
				<div style={{ margin: "2rem" }}>
					{tagNameList &&
						selectedTagName.map(tagName => (
							<Grid container spacing={2} key={tagName.id} sx={{ mb: 3 }}>
								<Grid item xs={8}>
									<span>{`${tagName.tag}   `}</span>
								</Grid>
								<Grid item xs={4}>
									{/* <Slider
										aria-label="Temperature"
										defaultValue={0}
										getAriaValueText={valuetext}
										valueLabelDisplay="on"
										step={5}
										marks
										min={-60}
										max={60}
										onChange={e =>
											handleSliderValue(tagName.id, e.target.value)
										}
									/> */}

									<div className="offset-number">
										{/* <button
											className="offset-minus"
											onClick={() => minusHandler(tagName.id)}
										>
											-
										</button> */}
										{/* <input type="text" value="0" /> */}
										<RemoveCircleIcon
											className="offset-minus"
											fontSize="large"
											onClick={() => minusHandler(tagName.id)}
										/>
										<TextField
											fullWidth
											size="small"
											type="tel"
											inputProps={{
												inputMode: "numeric",
												pattern: "[0-9]*",
												// sx: { height: 15 },
												style: { textAlign: "center", height: 15 },
											}}
											value={tagName.offset === undefined ? 0 : tagName.offset}
											onChange={e => {
												handleSliderValue(tagName.id, e.target.value);
											}}
											sx={{
												"& label": { paddingLeft: theme => theme.spacing(2) },
												"& fieldset": {
													borderRadius: "30px",
												},
											}}
											variant="outlined"
											className="offset-inputfield"
											error={false}
										/>

										{/* <button
											className="offset-plus"
											onClick={() => plusHandler(tagName.id)}
										>
											+
										</button> */}
										<AddCircleIcon
											className="offset-plus"
											fontSize="large"
											onClick={() => plusHandler(tagName.id)}
										/>
									</div>
								</Grid>
							</Grid>
						))}
				</div>

				<DialogActions
					sx={{
						display: "flex",
						justifyContent: "space-between",
						alignItems: "center",
					}}
				>
					<div>
						{" "}
						{warningMsg && warningMsg.warning ? (
							<Alert
								severity="warning"
								sx={{ borderRadius: "2rem", padding: "0 1rem" }}
							>
								Max offset can be {warningMsg.diff} minutes.
							</Alert>
						) : (
							""
						)}
					</div>
					<div style={{ paddingRight: "1.7rem" }}>
						<Button
							variant="outlined"
							onClick={handleReset}
							disabled={reset ? false : true}
							sx={{
								mr: 2,
								borderRadius: "2rem",
								height: "2rem",
								padding: "0 1.5rem",
								fontWeight: 600,
							}}
						>
							Reset
						</Button>
						<Button
							onClick={handleSubmit}
							variant="contained"
							sx={{
								borderRadius: "2rem",
								height: "2rem",
								fontWeight: 600,
								padding: "0 1.5rem",
								letterSpacing: "0.08em",
							}}
							disabled={warningMsg.warning ? true : false}
						>
							Submit
						</Button>
					</div>
				</DialogActions>
			</Dialog>
		</div>
	);
};

export default OffsetForm;
