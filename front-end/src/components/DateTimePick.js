import React, { useEffect, useState } from "react";
import {
	InputLabel,
	MenuItem,
	FormControl,
	Select,
	Checkbox,
	Container,
	TextField,
	FormHelperText,
	Button,
	CircularProgress,
	Grid,
	Autocomplete,
	Typography,
} from "@mui/material";
import TouchAppIcon from "@mui/icons-material/TouchApp";
import ErrorIcon from "@mui/icons-material/Error";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import EastSharpIcon from "@mui/icons-material/EastSharp";
import LineChart from "./LineChart";
import axios from "axios";
import OffsetForm from "./OffsetForm";
import moment from "moment";
import Paper from "@mui/material/Paper";
import { DateRangePicker, DatePicker, InputGroup } from "rsuite";

let allDataSets = [];
let datetimeError = "Invalid Date & Time";
let tagError = "Please Select Tag Names";
let zoneError = "Please Select Time Zone";
// let localHost = "http://localhost:3000/";
let localHost = "/";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

var apiUrl = (starttime, endtime, tagName, timezone) => {
	const URL = `${localHost}tagdata?format=MM/DD/YYYY,HH:mm:ss&tag=${tagName}&starttime=${starttime}&endtime=${endtime}&timezone=${timezone}`;

	return URL;
};

const getDateFormatted = (date, offsetValue) => {
	let currDate = moment(date);
	let value = offsetValue !== undefined ? offsetValue : 0;
	if (offsetValue < 0) {
		currDate = currDate.subtract(value, "m").format("MM/DD/YYYY HH:mm:ss");
	} else {
		currDate = currDate.add(value, "m").format("MM/DD/YYYY HH:mm:ss");
	}
	return currDate;
};

const DateTimePick = () => {
	// State Hooks

	const [tagNameState, settagNameState] = useState([]);
	const [apiData, setapiData] = useState([]);
	const [submitBtnDis, setsubmitBtnDis] = useState(false);
	const [loadingState, setloadingState] = useState("");
	const [tagNameList, settagNameList] = useState([]);
	const [timeZoneState, settimeZoneState] = useState([]);
	const [selectedTimeZone, setselectedTimeZone] = useState("");
	const [startDate, setstartDate] = useState("");
	const [endDate, setendDate] = useState("");
	const [validator, setvalidator] = useState({
		istagNameExist: true,
		isstartDateExist: true,
		isendDateExist: true,
		istimeZoneExist: true,
	});

	// const [charType, setchartType] = useState("Line");
	// const handleChange = event => {
	// setchartType(event.target.value);
	// };

	// Api Calls

	const tagListApi = async () => {
		const URL = `${localHost}taglist`;
		const res = await axios.get(URL);
		const arr = res.data.sort(
			(a, b) => new Date(a.timestamp) - new Date(b.timestamp)
		);
		settagNameList(arr);
	};

	const timeZone = async () => {
		const url = `${localHost}timezone`;
		const res = await axios.get(url);
		settimeZoneState(res.data);
	};
	const fetchApiData = (endPoints, allDataSets) => {
		const dataArr = [];
		axios.all(endPoints.map(endPoint => axios.get(endPoint))).then(
			axios.spread((...allDataSets) => {
				// console.log(allDataSets);
				setapiData(allDataSets);
			})
		);
	};

	// Event Handlers

	const handleTagNames = (event, value) => {
		settagNameState(value);
		setvalidator({
			...validator,
			istagNameExist: true,
		});
		setsubmitBtnDis(true);
	};

	const handleTimeZone = event => {
		setselectedTimeZone(event.target.value);
		setvalidator({
			...validator,
			istimeZoneExist: true,
		});
		setsubmitBtnDis(true);
	};

	const handlestartingDate = event => {
		const date_Time = moment(event).format();
		setstartDate(date_Time);
		setvalidator({
			...validator,
			isstartDateExist: true,
		});
		setsubmitBtnDis(true);
	};
	const handleendingDate = event => {
		const date_Time = moment(event).format();
		setendDate(date_Time);
		setvalidator({
			...validator,
			isendDateExist: true,
		});
		setsubmitBtnDis(true);
	};

	// Form Submit Handler

	const submitBtn = () => {
		setapiData([]);
		let istagNameExist = true;
		let isstartDateExist = true;
		let isendDateExist = true;
		let istimeZoneExist = true;
		let valid = true;
		const currDate = moment().subtract(30, "minutes").format();

		if (tagNameState.length === 0) {
			istagNameExist = false;
			valid = false;
			setloadingState("");
		}

		if (startDate.length === 0 || startDate > currDate) {
			isstartDateExist = false;
			valid = false;
			setloadingState("");
		}
		if (endDate.length === 0) {
			isendDateExist = false;
			setloadingState("");
			valid = false;
		}
		if (selectedTimeZone.length === 0) {
			istimeZoneExist = false;
			valid = false;
			setloadingState("");
		}
		if (startDate.length !== 0 && endDate.length !== 0) {
			if (new Date(endDate) < new Date(startDate)) {
				isendDateExist = false;
				isstartDateExist = false;
				// datetimeError = "Start Date is greater than End Date";
				valid = false;
				setloadingState("");
			}
		}

		if (valid) {
			const endPoints = tagNameState.map(tagName => {
				allDataSets.push(tagName.tag);
				return apiUrl(
					getDateFormatted(startDate, tagName.offset),
					// startDate.subtract(1, "h").format("MM/DD/YYYY HH:mm:ss"),
					// endDate.add(1, "h").format("MM/DD/YYYY HH:mm:ss"),
					getDateFormatted(endDate, tagName.offset),
					tagName.tag,
					selectedTimeZone
				);
			});
			fetchApiData(endPoints, allDataSets);
			setloadingState(<CircularProgress color="inherit" />);
			setsubmitBtnDis(false);
		}

		setvalidator({
			...validator,
			istagNameExist,
			isstartDateExist,
			isendDateExist,
			istimeZoneExist,
		});
	};

	const dateFieldStyle =
		validator.isstartDateExist === false || validator.isendDateExist === false
			? { borderColor: "red" }
			: {};

	useEffect(() => {
		tagListApi();
		timeZone();
	}, []);

	return (
		<Container sx={{ height: "100vh", minWidth: "100%" }}>
			<Grid
				container
				sx={{
					borderBottom: "3px solid #7F8487",
					padding: "2rem 0",
				}}
			>
				{/* Tag Names List */}
				<Grid item lg={3} md={3} xs={12} pb={2}>
					<Autocomplete
						sx={{ mr: 2 }}
						multiple
						label="Select"
						limitTags={1}
						id="multiple-limit-tags"
						size="small"
						options={tagNameList}
						disableCloseOnSelect
						getOptionLabel={name => name.tag}
						renderOption={(props, option, { selected }) => (
							<li {...props}>
								<Checkbox
									icon={icon}
									checkedIcon={checkedIcon}
									checked={selected}
								/>
								{option.tag}
							</li>
						)}
						renderInput={params => (
							<TextField
								{...params}
								label="Select Tag Names"
								placeholder="Select Tag Names"
								sx={{ fontSize: "0.8rem" }}
								error={validator.istagNameExist === false}
								helperText={
									validator.istagNameExist === false ? (
										<span style={{ display: "flex" }}>
											<ErrorIcon style={{ fontSize: "1rem" }} />
											{tagError}
										</span>
									) : (
										""
									)
								}
								InputLabelProps={{
									style: { fontSize: "0.95rem" },
								}}
							/>
						)}
						onChange={handleTagNames}
						ListboxProps={{
							sx: { fontSize: "0.75rem" },
						}}
					/>
				</Grid>

				{/* Offset Component */}

				<Grid item lg={5} md={5} xs={12} pb={2}>
					<div style={{ width: "100%" }}>
						<InputGroup
							style={{
								...dateFieldStyle,
								overflow: "hidden",
								display: "flex",
								justifyContent: "space-evenly",
								height: "2.5rem",
								alignItems: "center",
								position: "relative",
							}}
						>
							<DatePicker
								format="MM-dd-yyyy HH:mm"
								block
								appearance="default"
								placeholder="Enter Start Date & Time"
								onChange={handlestartingDate}
								style={{ width: "100%", height: "100%" }}
								onClean={() => {
									setstartDate(null);
									setvalidator({
										...validator,
										isstartDateExist: false,
									});
								}}
							/>
							<InputGroup.Addon>
								<EastSharpIcon
									fontSize="small"
									sx={{ color: "rgba(0, 0, 0, 0.6)" }}
								/>
							</InputGroup.Addon>
							<DatePicker
								format="MM-dd-yyyy HH:mm"
								block
								appearance="default"
								placeholder="Enter End Date & Time"
								onChange={handleendingDate}
								style={{ width: "100%", height: "100%" }}
								onClean={() => {
									setendDate(null);
									setvalidator({
										...validator,
										isendDateExist: false,
									});
								}}
							/>
						</InputGroup>
						<div
							style={{
								display: "flex",
								justifyContent: "space-around",
								alignItems: "center",
							}}
						>
							{validator.isstartDateExist === false ||
							validator.isendDateExist === false ? (
								<FormHelperText sx={{ color: "red" }}>
									<ErrorIcon style={{ fontSize: "1rem" }} />
									<span>{datetimeError}</span>
								</FormHelperText>
							) : (
								""
							)}
						</div>
					</div>
				</Grid>

				{/* Zone Input Selector */}
				<Grid item lg={3} md={3} xs={12} pl={2} pr={2} pb={2}>
					<FormControl
						sx={{ textAlign: "left" }}
						size="small"
						error={validator.istimeZoneExist === false}
						fullWidth
						className="timezone-div"
					>
						<InputLabel
							id="demo-simple-select-autowidth-label"
							sx={{ fontSize: "0.9rem" }}
						>
							Select Time Zone
						</InputLabel>
						<Select
							labelId="demo-simple-select-autowidth-label"
							id="demo-simple-select-autowidth"
							value={selectedTimeZone}
							fullWidth
							label="Select Time Zone"
							onChange={handleTimeZone}
							sx={{
								fontSize: "0.9rem",
								height: "40px",
							}}
						>
							{timeZoneState &&
								timeZoneState.map((zoneName, i) => (
									<MenuItem
										sx={{ fontSize: "0.8rem" }}
										key={i}
										value={zoneName.timeZone}
									>
										{zoneName.zone}
									</MenuItem>
								))}
						</Select>
						{validator.istimeZoneExist === false ? (
							<FormHelperText>
								<ErrorIcon style={{ fontSize: "1rem" }} /> {zoneError}
							</FormHelperText>
						) : (
							""
						)}
					</FormControl>
				</Grid>

				<Grid item lg={1} md={1} xs={12} pb={2}>
					<Button
						size="small"
						variant="contained"
						onClick={submitBtn}
						disabled={submitBtnDis === false ? true : false}
						sx={{
							height: "2rem",
							width: 120,
							borderRadius: "1rem",
							fontSize: "1rem",
							fontWeight: 600,
							letterSpacing: "0.09em",
							mr: 2,
						}}
					>
						Submit
					</Button>
				</Grid>
			</Grid>

			{/* Chart JS Component */}
			<div style={{ paddingBottom: "3rem" }}>
				<Paper
					elevation={24}
					sx={{
						width: "90%",
						height: "auto",
						margin: "2rem auto",
						padding: "1rem",
					}}
				>
					{apiData && apiData.length > 0 ? (
						<LineChart
							apiData={apiData}
							tagNameState={tagNameState}
							startDate={startDate}
							endDate={endDate}
							submitHandler={submitBtn}
						/>
					) : (
						<div
							style={{
								height: "80vh",
								width: "100%",
								display: "flex",
								justifyContent: "center",
								alignItems: "center",
								flexDirection: "column",
							}}
						>
							{/* {apiData.length == 2 ? <span>Data Not Found</span> : ""} */}
							{loadingState ? (
								<span>
									{loadingState} <h4>Please Wait ....</h4>
								</span>
							) : (
								<h4>
									Please Enter Details & Click {"   "}
									<TouchAppIcon /> on Submit...
								</h4>
							)}
						</div>
					)}
				</Paper>
			</div>
		</Container>
	);
};

export default DateTimePick;
