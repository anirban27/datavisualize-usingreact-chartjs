import React, { useState, useEffect, useRef } from "react";
import { FormControlLabel, Switch, Button } from "@mui/material";
import CloudDownloadIcon from "@mui/icons-material/CloudDownload";
import { Line } from "react-chartjs-2";
import zoomPlugin from "chartjs-plugin-zoom";
import OffsetForm from "./OffsetForm";
import { saveAs } from "file-saver";
import { bgPlugin } from "../assets/lib/function";
import IconButton from "@mui/material/IconButton";
import RotateLeftIcon from "@mui/icons-material/RotateLeft";
import { Tooltip as TooltipMui } from "@mui/material";
import {
	Chart as ChartJS,
	Title,
	Tooltip,
	LineElement,
	Legend,
	CategoryScale,
	LinearScale,
	PointElement,
	Filler,
	TimeScale,
} from "chart.js";

ChartJS.register(
	Title,
	Tooltip,
	LineElement,
	Legend,
	CategoryScale,
	LinearScale,
	PointElement,
	Filler,
	TimeScale,
	zoomPlugin,
	bgPlugin
);

// random Color Generator function

const randColor = () => {
	return (
		"#" +
		Math.floor(Math.random() * 16777215)
			.toString(16)
			.padStart(6, "0")
			.toUpperCase()
	);
};

// const randColor = () => {
// 	var color = "#";
// 	for (var i = 0; i < 6; i++) {
// 		color += Math.floor(Math.random() * 10);
// 	}
// 	return color;
// };

// Chart DataSets Function

const axisDataSets = (dataSetArr, absValue) =>
	dataSetArr.map((e, i) => {
		const color = randColor();
		const obj = {
			label: e.data[0].tag === undefined ? "" : e.data[0].tag,
			data: e.data.map(dataObj =>
				absValue ? dataObj.numvaluenorm : dataObj.numvalue
			),
			borderColor: color,
			backgroundColor: color,
			fill: false,
			tension: 0.5,
			pointBorderColor: color,
			borderWidth: 2,
			pointRadius: 0.8,
		};
		return obj;
	});

// ChartJS Config Options

const options = {
	normalized: true,
	elements: {
		point: {
			radius: 0,
		},
	},
	spanGaps: true,
	animation: false,
	responsive: true,
	maintainAspectRatio: false,
	legend: {
		labels: {
			fontColor: "blue",
			fontSize: 18,
		},
	},
	scales: {
		x: {
			distribution: "linear",
			grid: {
				drawOnChartArea: false,
				tickWidth: 1,
				tickLength: 10,
				offset: true,
			},
			display: true,
			ticks: {
				align: "center",
				maxRotation: 90,
				minRotation: 90,
				align: "start",
			},
		},
		y: {
			grid: {
				tickWidth: 1,
				drawBorder: false,
			},
		},
	},
	plugins: {
		background: {
			color: "white",
		},
		zoom: {
			zoom: {
				drag: {
					enabled: true,
					backgroundColor: "rgba(176, 176, 176, 0.7)",
				},
				mode: "x",
			},
			pan: {
				enabled: true,
				mode: "x",
				modifierKey: "shift",
			},
			mode: "xy",
		},
		legend: {
			display: true,
			useBorderRadius: true,
			borderRadius: 2,
		},
		tooltip: {
			bodyFont: {
				font: {
					size: 14,
					style: "normal",
				},
			},
			callbacks: {
				beforeTitle: function (context) {
					return `Date & Time : ${context[0].label}`;
				},
				afterTitle: function (context) {
					return `Value : ${context[0].formattedValue}`;
				},
				labelPointStyle: function (context) {
					return {
						pointStyle: "circle",
						rotation: 0,
					};
				},
				label: function (context) {
					return `${context.dataset.label}`;
				},
				labelTextColor: function (context) {
					return "#000";
				},
				title: () => null,
			},
			cornerRadius: 1,
			backgroundColor: "#D8D9CF",
			titleColor: "#000",
			titleMarginBottom: 8,
			titleSpacing: 3,
			bodyColor: "#475059",
			borderColor: "#E3E3E3",
			borderWidth: 1,
			padding: 10,
			bodySpacing: 8,
			usePointStyle: true,
		},
	},
	transitions: {
		zoom: {
			animation: {
				duration: 1000,
				easing: "easeOutCubic",
			},
		},
	},
	animation: {
		duration: 0,
	},
	hover: {
		animationDuration: 0,
	},
	responsiveAnimationDuration: 0,
};

const LineChart = ({
	apiData,
	tagNameState,
	startDate,
	endDate,
	submitHandler,
}) => {
	// State variable Hooks
	const [data, setdata] = useState(null);
	const [absValue, setabsValue] = useState(false);
	const chartRef = useRef(null);

	// Event Handler Functions

	var timeStamp = apiData[0].data.map(e => e.timestamp.replace(/,/gi, " "));

	const obj = {
		labels: timeStamp,
		datasets: [...axisDataSets(apiData, absValue)],
	};

	const handleResetZoom = () => {
		if (chartRef && chartRef.current) {
			chartRef.current.resetZoom();
		}
	};

	const handleDownloadPdf = () => {
		const canvasSave = document.getElementById("lineChart");
		canvasSave.toBlob(function (blob) {
			saveAs(blob, "tag-graph.png");
		});
	};

	const absSwitchHandler = () => setabsValue(!absValue);

	useEffect(() => {
		if (apiData || absValue) {
			setdata(obj);
		}
	}, [absValue]);

	return (
		<div
			style={{
				margin: "0 auto",
				padding: "1rem",
				marginBottom: "3rem",
				width: "100%",
				height: "80vh",
			}}
		>
			<div
				style={{
					display: "flex",
					justifyContent: "space-between",
					alignItems: "center",
				}}
			>
				<OffsetForm
					tagNameList={tagNameState}
					selectedTagName={tagNameState}
					submitHandler={submitHandler}
					startDate={startDate}
				/>
				<div
					style={{
						display: "flex",
						justifyContent: "flex-end",
						alignItems: "center",
					}}
					className="chart-navigate-bar"
				>
					{" "}
					<Button
						variant="outlined"
						sx={{
							height: "2rem",
							borderRadius: "1rem",
							fontSize: "0.87rem",
							fontWeight: 600,
							border: "2px solid #1976d2",
							mr: 2,
						}}
						onClick={() => absSwitchHandler()}
					>
						{absValue ? "Normalised Plot" : "Absolute Plot"}
						<FormControlLabel
							control={
								<Switch
									checked={absValue}
									onChange={absSwitchHandler}
									name="absSwitch"
									size="small"
								/>
							}
							sx={{ ml: 1, mr: -1 }}
						/>
					</Button>
					<Button
						variant="outlined"
						onClick={() => handleResetZoom()}
						sx={{
							height: "2rem",
							borderRadius: "1rem",
							fontSize: "0.87rem",
							fontWeight: 600,
							border: "2px solid #1976d2",
							mr: 2,
						}}
					>
						Reset Zoom
						<RotateLeftIcon fontSize="small" sx={{ ml: 1 }} />
					</Button>
					<TooltipMui title="Download png" arrow>
						<IconButton onClick={() => handleDownloadPdf()}>
							<CloudDownloadIcon
								sx={{ color: "#1976d2", cursor: "pointer" }}
								fontSize="large"
							/>
						</IconButton>
					</TooltipMui>
				</div>
			</div>

			<div style={{ height: "100%" }}>
				{data && (
					<Line ref={chartRef} data={data} options={options} id="lineChart">
						LineChart
					</Line>
				)}
			</div>
		</div>
	);
};

export default LineChart;
